/*
* Copyright (c) 2016 Carnegie Mellon University, Author <ar.sankalp@gmail.com>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <trajectory_visualization/Trajectory.h>

ros::Publisher marker_pub;
visualization_msgs::Marker line_strip;
std::string ns;
double r,g,b,scale;

void TrajectoryCallback ( const trajectory_visualization::Trajectory::ConstPtr& traj_msg )
{

   line_strip.header.frame_id =  traj_msg->header.frame_id;
   line_strip.header.stamp = traj_msg->header.stamp;
   line_strip.ns = ns;
   line_strip.action = visualization_msgs::Marker::ADD;
   line_strip.type = visualization_msgs::Marker::LINE_STRIP;
   line_strip.pose.orientation.w = 1.0;

   line_strip.id = 1;

   line_strip.scale.x = scale;
   line_strip.color.r = r;
   line_strip.color.g = g;
   line_strip.color.b = b;
   line_strip.color.a = 1.0;

   geometry_msgs::Point p;
   for(size_t i=0;i<traj_msg->trajectory.size();i++){
   p.x = traj_msg->trajectory[i].position.x;
   p.y = traj_msg->trajectory[i].position.y;
   p.z = traj_msg->trajectory[i].position.z;
   line_strip.points.push_back ( p );
   }
   marker_pub.publish ( line_strip );
   line_strip.points.clear();
}

int main ( int argc, char **argv )
{
   ros::init ( argc, argv, "trajectory_visualizer" );
   ros::NodeHandle np ( "~" );
   std::string trajectory_topic;
   std::string marker_topic;
   bool got_params = true;
   got_params = got_params && np.getParam("trajectory_topic",trajectory_topic);
   got_params = got_params && np.getParam("marker_topic",marker_topic);
   got_params = got_params && np.getParam( "r", r);
   got_params = got_params && np.getParam( "g", g);
   got_params = got_params && np.getParam( "b", b);
   got_params = got_params && np.getParam( "scale", scale);
   got_params = got_params && np.getParam( "namespace", ns);
   if(!got_params){
       ROS_ERROR_STREAM("Trajectory visualization did not get all the parameters.");
       exit(-1);
   }
   ros::Subscriber sub = np.subscribe ( trajectory_topic, 100, TrajectoryCallback );
   marker_pub = np.advertise<visualization_msgs::Marker> ( marker_topic, 10 );

   ros::spin();

   return 0;
}
